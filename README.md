[![Runbot Status](https://runbot.odoo-community.org/runbot/badge/flat/264/12.0.svg)](https://runbot.odoo-community.org/runbot/repo/github-com-oca-field-service-264)
[![Build Status](https://travis-ci.org/OCA/field-service.svg?branch=12.0)](https://travis-ci.org/OCA/field-service)

# Field Service

[Field service management](https://en.wikipedia.org/wiki/Field_service_management) (FSM) coordinates company resources employed at, or en route to, client sites, rather than on the company's premises. FSM most commonly refers to companies who need to manage installation, service or repairs of systems or equipment.

Examples of field service use cases are:

- In telecommunications and cable industry, technicians who install cable or run phone lines into residences or business establishments.
- In healthcare, mobile nurses who provide in-home care for elderly or disabled.
- In gas utilities, engineers who are dispatched to investigate and repair suspected leaks.
- In heavy engineering, mining, industrial and manufacturing, technicians dispatched for preventative maintenance and repair.
- In property maintenance, including landscaping, irrigation, and home and office cleaning.
- In HVAC industry, technicians have the expertise and equipment to investigate units in residential, commercial and industrial environments.

[//]: # (addons)

Available addons
----------------
addon | version | summary
--- | --- | ---
[fieldservice](fieldservice/) | 12.0.1.3.0 | Manage Field Service Locations, Workers and Orders
[fieldservice_account](fieldservice_account/) | 12.0.2.0.1 | Track employee time and invoice for Field Service Orders
[fieldservice_agreement](fieldservice_agreement/) | 12.0.1.0.0 | Manage FSM Agreements
[fieldservice_crm](fieldservice_crm/) | 12.0.1.0.1 | CRM Opportunity integration for Field Service
[fieldservice_delivery](fieldservice_delivery/) | 12.0.1.0.0 | Delivery and Carriers for Field Services
[fieldservice_distribution](fieldservice_distribution/) | 12.0.1.0.0 | Manage your distribution structure
[fieldservice_geoengine](fieldservice_geoengine/) | 12.0.1.0.0 | Extend field service functionality based on Geoengine
[fieldservice_isp_flow](fieldservice_isp_flow/) | 12.0.1.0.0 | FSM Stages for Internet Service Providers
[fieldservice_maintenance](fieldservice_maintenance/) | 12.0.1.0.0 | Maintenance
[fieldservice_partner_multi_relation](fieldservice_partner_multi_relation/) | 12.0.1.0.0 | Field Service Partner Relations
[fieldservice_project](fieldservice_project/) | 12.0.1.0.0 | Create field service orders from a project or project task
[fieldservice_purchase](fieldservice_purchase/) | 12.0.1.0.0 | Manage FSM Purchases
[fieldservice_recurring](fieldservice_recurring/) | 12.0.1.0.0 | Manage recurring field service works
[fieldservice_repair](fieldservice_repair/) | 12.0.1.0.0 | Integrate FSM orders with MRP repair orders
[fieldservice_sale](fieldservice_sale/) | 12.0.1.1.0 | Sell field services.
[fieldservice_sale_recurring](fieldservice_sale_recurring/) | 12.0.1.0.0 | Sell recurring field services.
[fieldservice_skill](fieldservice_skill/) | 12.0.1.0.0 | Manage your FS workers skills
[fieldservice_stage_server_action](fieldservice_stage_server_action/) | 12.0.1.0.0 | Add Server Actions based on FSM Stage
[fieldservice_stock](fieldservice_stock/) | 12.0.1.0.1 | Inventory and Stock Operations for Field Services
[fieldservice_stock_account](fieldservice_stock_account/) | 12.0.1.0.0 | Adds inventory items delivered as part of the field service order to a customer invoice
[fieldservice_stock_account_analytic](fieldservice_stock_account_analytic/) | 12.0.1.0.0 | Track costs of delivered items with the analytic accounting
[fieldservice_substatus](fieldservice_substatus/) | 12.0.1.1.0 | Add sub-statuses to Field Service orders
[fieldservice_vehicle](fieldservice_vehicle/) | 12.0.1.0.0 | manage your field service vehicles
[fieldservice_vehicle_stock](fieldservice_vehicle_stock/) | 12.0.1.0.0 | Inventory Operations for Field Service with Vehicles

[//]: # (end addons)

## Translation Status

[![Translation status](https://translation.odoo-community.org/widgets/field-service-12-0/-/multi-auto.svg)](https://translation.odoo-community.org/engage/field-service-12-0/?utm_source=widget)

----

OCA, or the [Odoo Community Association](http://odoo-community.org/), is a nonprofit organization whose
mission is to support the collaborative development of Odoo features and
promote its widespread use.
